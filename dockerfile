FROM ubuntu:latest
RUN apt-get update -y
COPY test/pingcount.sh /scripts/pingcount.sh
RUN ["chmod", "-x", "/scripts/pingcount.sh"]